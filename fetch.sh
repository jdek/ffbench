#!/bin/sh

for name in alpha beta charlie; do
  for s in 960x540 1280x720 1920x1080 3840x2160; do
    for depth in yuv420p yuv420p10 yuv410p12; do
      wget -c "http://34.217.48.208/samples/nocturne_${name}_${s}_${depth}.mkv"
    done
  done
done
