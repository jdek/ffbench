#!/bin/bash -e

FFDIR=ffmpeg
FFOBJ=$FFDIR/bench-obj
FFMPEG=$FFOBJ/ffmpeg_g
FFPROBE=$FFOBJ/ffprobe_g

REV=$(cd "$FFDIR" && git rev-parse --short HEAD)
[ -f NAME ] || { echo "No NAME file, please create this file with the bench platform name"; return 1; }
NAME=$(cat NAME)
BENCH_NAME="$NAME-$REV"

ADIR="$PWD/$BENCH_NAME"
[ -d "$ADIR" ] || mkdir -p "$ADIR"

perf_report(){
  REPORT="$ADIR/perf_$1.info.txt"
 cat <<EOF > "$REPORT"
$(sha256sum $2)

$($FFPROBE -hide_banner "$2" 2>&1)
EOF
  perf report > "$ADIR/perf_$1.txt"
}

perf_setup() {
  [ -f "$ADIR/perf_$1.txt" ] && return 1
  rm -f perf.data
  set -x
}

perf_end() {
  set +x
  perf_report "$1" "$2"
}

perf_decode() {
  perf_setup "$1" || return 0
  #perf stat -e cycles,instructions,cache-misses -r 5 $FFPROBE "$2" -f null - > "$ADIR/perf_$1.stat.txt"
  perf record -e cpu-cycles $FFMPEG -i "$2" -f null -
  perf_end "$1" "$2"
}

perf_encode1() {
  perf_setup "$1" || return 0
  perf record $FFMPEG -vstats_file vstats.log -threads 2 -noautorotate -i "$2" \
    -filter_complex "[0:0]split[c1][c7];[c1]framestep=2[c2];[c2]fps=fps=15[c4];[c4]scale=w=+256:h=+144[c5];[c5]setsar=1[c6];[c7]fps=fps=30[c9];[c9]split=6[c10][c13][c16][c19][c22][c25];[c10]scale=w=+256:h=+144[c11];[c11]setsar=1[c12];[c13]scale=w=+426:h=+240[c14];[c14]setsar=1[c15];[c16]scale=w=+640:h=+360[c17];[c17]setsar=1[c18];[c19]scale=w=+854:h=+480[c20];[c20]setsar=1[c21];[c22]scale=w=+1280:h=+720[c23];[c23]setsar=1[c24];[c25]scale=w=+1920:h=+1080[c26];[c26]setsar=1[c27]" \
    -map_metadata -1 -pass:v:0 1 -passlogfile:v:0 pass_0 -b:v:0 30000 -vsync 1 -color_primaries 1 -color_trc 1 -colorspace 1 -color_range 1 -g 120 -bf 6 -coder 1 -threads 2 -crf 23.5 -x264-params "stitchable=1:b-bias=1:deblock=0,0:direct-pred=auto:cplxblur=20:aq-mode=1:rc-lookahead=7:aq-strength=1:b-pyramid=0:weightb=1:mixed-refs=1:8x8dct=0:fast-pskip=1:ssim=1" -qmin 15 -qmax 45 -refs 3 -codec:v:0 libx264 -an -sn -f null -map [c6] -y /dev/null \
    -map_metadata -1 -pass:v:0 1 -passlogfile:v:0 pass_1 -b:v:0 97000 -vsync 1 -color_primaries 1 -color_trc 1 -colorspace 1 -color_range 1 -g 240 -bf 6 -coder 1 -threads 2 -crf 23.5 -x264-params "stitchable=1:b-bias=1:deblock=0,0:direct-pred=auto:cplxblur=20:aq-mode=1:rc-lookahead=7:aq-strength=1:b-pyramid=0:weightb=1:mixed-refs=1:8x8dct=0:fast-pskip=1:ssim=1" -qmin 15 -qmax 45 -refs 3 -codec:v:0 libx264 -an -sn -f null -map [c12] -y /dev/null \
    -map_metadata -1 -pass:v:0 1 -passlogfile:v:0 pass_2 -b:v:0 219000 -vsync 1 -color_primaries 1 -color_trc 1 -colorspace 1 -color_range 1 -g 240 -bf 6 -coder 1 -threads 2 -crf 28 -x264-params "stitchable=1:b-bias=1:deblock=0,0:direct-pred=auto:cplxblur=20:aq-mode=1:rc-lookahead=7:aq-strength=1:b-pyramid=0:weightb=1:mixed-refs=1:8x8dct=0:fast-pskip=1:ssim=1" -qmin 15 -qmax 45 -refs 3 -codec:v:0 libx264 -an -sn -f null -map [c15] -y /dev/null \
    -map_metadata -1 -pass:v:0 1 -passlogfile:v:0 pass_3 -b:v:0 366000 -vsync 1 -color_primaries 1 -color_trc 1 -colorspace 1 -color_range 1 -g 240 -bf 6 -coder 1 -threads 2 -crf 28 -x264-params "stitchable=1:b-bias=1:deblock=0,0:direct-pred=auto:cplxblur=20:aq-mode=1:rc-lookahead=7:aq-strength=1:b-pyramid=0:weightb=1:mixed-refs=1:8x8dct=0:fast-pskip=1:ssim=1" -qmin 15 -qmax 45 -refs 3 -codec:v:0 libx264 -an -sn -f null -map [c18] -y /dev/null \
    -map_metadata -1 -pass:v:0 1 -passlogfile:v:0 pass_4 -b:v:0 930000 -vsync 1 -color_primaries 1 -color_trc 1 -colorspace 1 -color_range 1 -g 240 -bf 4 -coder 1 -threads 2 -crf 28 -x264-params "stitchable=1:b-bias=1:deblock=0,0:direct-pred=auto:cplxblur=20:aq-mode=1:rc-lookahead=7:aq-strength=1:b-pyramid=0:weightb=1:mixed-refs=1:8x8dct=0:fast-pskip=1:ssim=1" -qmin 15 -qmax 45 -refs 3 -codec:v:0 libx264 -an -sn -f null -map [c21] -y /dev/null \
    -map_metadata -1 -pass:v:0 1 -passlogfile:v:0 pass_5 -b:v:0 1885000 -vsync 1 -color_primaries 1 -color_trc 1 -colorspace 1 -color_range 1 -g 240 -bf 4 -coder 1 -threads 2 -crf 28 -x264-params "stitchable=1:b-bias=1:deblock=0,0:direct-pred=auto:cplxblur=20:aq-mode=1:rc-lookahead=7:aq-strength=1:b-pyramid=0:weightb=1:mixed-refs=1:8x8dct=1:fast-pskip=1:ssim=1" -qmin 15 -qmax 45 -refs 3 -codec:v:0 libx264 -an -sn -f null -map [c24] -y /dev/null \
    -map_metadata -1 -pass:v:0 1 -passlogfile:v:0 pass_6 -b:v:0 3630000 -vsync 1 -color_primaries 1 -color_trc 1 -colorspace 1 -color_range 1 -g 240 -bf 3 -coder 1 -threads 2 -crf 28 -x264-params "stitchable=1:b-bias=1:deblock=0,0:direct-pred=auto:cplxblur=20:aq-mode=1:rc-lookahead=7:aq-strength=1:b-pyramid=0:weightb=1:mixed-refs=1:8x8dct=1:fast-pskip=1:ssim=1" -qmin 15 -qmax 45 -refs 3 -codec:v:0 libx264 -an -sn -f null -map [c27] -y /dev/null
  rm pass*.log
  rm *.mbtree
  perf_end "$1" "$2"
}

perf_encode2() {
  perf_setup "$1" || return 0
  perf record $FFMPEG -vstats_file vstats.log -threads 2 -noautorotate -i "$2" \
    -filter_complex "[0:0]split[c1][c7];[c1]framestep=2[c2];[c2]fps=fps=15[c4];[c4]scale=w=+256:h=+144[c5];[c5]setsar=1[c6];[c7]fps=fps=30[c9];[c9]split=6[c10][c13][c16][c19][c22][c25];[c10]scale=w=+256:h=+144[c11];[c11]setsar=1[c12];[c13]scale=w=+426:h=+240[c14];[c14]setsar=1[c15];[c16]scale=w=+640:h=+360[c17];[c17]setsar=1[c18];[c19]scale=w=+854:h=+480[c20];[c20]setsar=1[c21];[c22]scale=w=+1280:h=+720[c23];[c23]setsar=1[c24];[c25]scale=w=+1920:h=+1080[c26];[c26]setsar=1[c27]" \
    -map_metadata -1 -pass:v:0 1 -passlogfile:v:0 pass_0 -b:v:0 30000 -vsync 1 -color_primaries 1 -color_trc 1 -colorspace 1 -color_range 1 -g 120 -bf 6 -coder 1 -threads 2 -crf 23.5 -x265-params "stitchable=1:b-bias=1:deblock=0,0:direct-pred=auto:cplxblur=20:aq-mode=1:rc-lookahead=7:aq-strength=1:b-pyramid=0:weightb=1:mixed-refs=1:8x8dct=0:fast-pskip=1:ssim=1" -qmin 15 -qmax 45 -refs 3 -codec:v:0 libx265 -an -sn -f null -map [c6] -y /dev/null \
    -map_metadata -1 -pass:v:0 1 -passlogfile:v:0 pass_1 -b:v:0 97000 -vsync 1 -color_primaries 1 -color_trc 1 -colorspace 1 -color_range 1 -g 240 -bf 6 -coder 1 -threads 2 -crf 23.5 -x265-params "stitchable=1:b-bias=1:deblock=0,0:direct-pred=auto:cplxblur=20:aq-mode=1:rc-lookahead=7:aq-strength=1:b-pyramid=0:weightb=1:mixed-refs=1:8x8dct=0:fast-pskip=1:ssim=1" -qmin 15 -qmax 45 -refs 3 -codec:v:0 libx265 -an -sn -f null -map [c12] -y /dev/null \
    -map_metadata -1 -pass:v:0 1 -passlogfile:v:0 pass_2 -b:v:0 219000 -vsync 1 -color_primaries 1 -color_trc 1 -colorspace 1 -color_range 1 -g 240 -bf 6 -coder 1 -threads 2 -crf 28 -x265-params "stitchable=1:b-bias=1:deblock=0,0:direct-pred=auto:cplxblur=20:aq-mode=1:rc-lookahead=7:aq-strength=1:b-pyramid=0:weightb=1:mixed-refs=1:8x8dct=0:fast-pskip=1:ssim=1" -qmin 15 -qmax 45 -refs 3 -codec:v:0 libx265 -an -sn -f null -map [c15] -y /dev/null \
    -map_metadata -1 -pass:v:0 1 -passlogfile:v:0 pass_3 -b:v:0 366000 -vsync 1 -color_primaries 1 -color_trc 1 -colorspace 1 -color_range 1 -g 240 -bf 6 -coder 1 -threads 2 -crf 28 -x265-params "stitchable=1:b-bias=1:deblock=0,0:direct-pred=auto:cplxblur=20:aq-mode=1:rc-lookahead=7:aq-strength=1:b-pyramid=0:weightb=1:mixed-refs=1:8x8dct=0:fast-pskip=1:ssim=1" -qmin 15 -qmax 45 -refs 3 -codec:v:0 libx265 -an -sn -f null -map [c18] -y /dev/null \
    -map_metadata -1 -pass:v:0 1 -passlogfile:v:0 pass_4 -b:v:0 930000 -vsync 1 -color_primaries 1 -color_trc 1 -colorspace 1 -color_range 1 -g 240 -bf 4 -coder 1 -threads 2 -crf 28 -x265-params "stitchable=1:b-bias=1:deblock=0,0:direct-pred=auto:cplxblur=20:aq-mode=1:rc-lookahead=7:aq-strength=1:b-pyramid=0:weightb=1:mixed-refs=1:8x8dct=0:fast-pskip=1:ssim=1" -qmin 15 -qmax 45 -refs 3 -codec:v:0 libx265 -an -sn -f null -map [c21] -y /dev/null \
    -map_metadata -1 -pass:v:0 1 -passlogfile:v:0 pass_5 -b:v:0 1885000 -vsync 1 -color_primaries 1 -color_trc 1 -colorspace 1 -color_range 1 -g 240 -bf 4 -coder 1 -threads 2 -crf 28 -x265-params "stitchable=1:b-bias=1:deblock=0,0:direct-pred=auto:cplxblur=20:aq-mode=1:rc-lookahead=7:aq-strength=1:b-pyramid=0:weightb=1:mixed-refs=1:8x8dct=1:fast-pskip=1:ssim=1" -qmin 15 -qmax 45 -refs 3 -codec:v:0 libx265 -an -sn -f null -map [c24] -y /dev/null \
    -map_metadata -1 -pass:v:0 1 -passlogfile:v:0 pass_6 -b:v:0 3630000 -vsync 1 -color_primaries 1 -color_trc 1 -colorspace 1 -color_range 1 -g 240 -bf 3 -coder 1 -threads 2 -crf 28 -x265-params "stitchable=1:b-bias=1:deblock=0,0:direct-pred=auto:cplxblur=20:aq-mode=1:rc-lookahead=7:aq-strength=1:b-pyramid=0:weightb=1:mixed-refs=1:8x8dct=1:fast-pskip=1:ssim=1" -qmin 15 -qmax 45 -refs 3 -codec:v:0 libx265 -an -sn -f null -map [c27] -y /dev/null
  rm pass*.log
  rm *.mbtree
  perf_end "$1" "$2"
}

perf_encode1 "encode_x264_first_pass_1080p25" samples/pedestrian_area_1080p25.y4m
perf_encode2 "encode_x265_first_pass_1080p25" samples/pedestrian_area_1080p25.y4m

perf_decode "decode_h264_high_1080i25_mbaff" samples/beyonce.at.the.bbc.1080mbaff.sample.ts
perf_decode "decode_h264_high_1080i25_paff" samples/rugby_1080i_paff.ts
perf_decode "decode_h264_high10_1080p" samples/bakemonogatari_ep04_1080p_h264_hi10_cut.ts
perf_decode "decode_h264_high_2160p30" samples/bbb_sunflower_2160p_30fps_normal_cut.ts
perf_decode "decode_h264_baseline_1080p30" samples/factory_1080p30_cavlc_baseline.ts

perf_decode "decode_hevc_main_1080p30" samples/jellyfish-40-mbps-hd-hevc.mkv
perf_decode "decode_hevc_main10_1080p30" samples/jellyfish-40-mbps-hd-hevc-10bit.mkv
perf_decode "decode_hevc_main10_2160p60" samples/20160621T150804-02-102768_HEVC_4K.ts

perf_decode "decode_vp9_prof0_8bit_1080p24" samples/eye_of_the_storm_vp9_1080p24.webm
perf_decode "decode_vp9_prof0_8bit_2160p24" samples/eye_of_the_storm_vp9_2160p24.webm
perf_decode "decode_vp9_prof2_10bit_2160p24" samples/the_redwoods_vp9_prof2_10bit_2160p24_cut.mkv

perf_decode "decode_unknown" samples/ch129_billiard.ts

perf_setup "null"
perf record $FFMPEG -i samples/factory_1080p30.y4m -pix_fmt nv12 -f null -
perf_end "scale_yuv420p_to_nv12" samples/factory_1080p30.y4m

rm -f perf.data
