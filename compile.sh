#!/bin/sh -e
# vim: ts=4:et

PREFIX=$PWD/prefix

prefix_rebuild=
while getopts r opt; do
    case $OPT in
    r)  prefix_rebuild=true;;
    ?)  printf "Usage: %s: [-r]\n" $0
        exit 2;;
    esac
done
shift $(($OPTIND - 1))

num_cpus()
{
    getconf _NPROCESSORS_ONLN
}

command_exists ()
{
    type "$1" >/dev/null 2>&1
}

clone_git_repo()
{
    host=$1
    user=$2
    repo=$3
    branch=${4:-master}
    sha=${5:-HEAD}

    OLDPWD=$PWD
    [ -d $repo/.git ] && { cd $repo && git pull; cd $OLDPWD; } || rm -rf $OLDPWD/$repo

    [ -d $repo ] || SSH_ASKPASS=false git clone --recursive --depth 1 -b $branch git@$host:$user/$repo.git $repo < /dev/null || \
                    SSH_ASKPASS=false git clone --recursive --depth 1 -b $branch https://$host/$user/$repo.git $repo < /dev/null || return 1
    [ "$sha" = "HEAD" ] || { cd $repo && git fetch origin $sha && git checkout $sha; cd $OLDPWD; }
}

x265=true
if ! command_exists cmake; then
    echo >&2 "No Cmake installed, skipping x265"
    x265=
fi

clone_git_repo github.com ffmpeg ffmpeg master 4de4bc06fdfd0383f3d9012c6557a38408a09d28
clone_git_repo github.com mirror x264 master ae03d92b52bb7581df2e75d571989cb1ecd19cbd
clone_git_repo github.com videolan x265 master 419182243fb2e2dfbe91dfc45a51778cf704f849
clone_git_repo github.com webmproject libvpx master 59c9e1d87ef33bc82fca82cfcf5202d4b86c92e7

if $prefix_rebuild; then
    rm -rf $PREFIX
fi

cd x264
rm -rf bench-obj; mkdir bench-obj; cd bench-obj
../configure --enable-static --disable-shared --prefix=$PREFIX
make -j$(num_cpus) install
cd ../..

if $x265; then
cd x265;
rm -rf bench-obj; mkdir bench-obj; cd bench-obj
cat <<EOF > "$PREFIX/lib/pkgconfig/x265.pc"
prefix=$PREFIX
exec_prefix=\${prefix}
libdir=\${exec_prefix}/lib
includedir=\${prefix}/include

Name: x265
Description: H.265/HEVC video encoder
Version: 3.4
Libs: -L\${libdir} -lx265 -lstdc++ -lm -ldl -pthread
Cflags: -I\${includedir}
EOF
    cmake ../source -DNATIVE_BUILD=ON -DCMAKE_INSTALL_PREFIX=$PREFIX
    make -j$(num_cpus) install
cd ../..
fi

cd libvpx
rm -rf bench-obj; mkdir bench-obj; cd bench-obj
../configure --enable-static --disable-shared --enable-vp8 --enable-vp9 --enable-debug-libs --prefix=$PREFIX
make -j$(num_cpus) install
cd ../..

cd ffmpeg
rm -rf bench-obj; mkdir bench-obj; cd bench-obj
PKG_CONFIG_PATH=$PREFIX/lib/pkgconfig ../configure --enable-static --disable-shared --enable-gpl \
                                                   --extra-ldflags=-L$PREFIX/lib --extra-cflags=-I$PREFIX/include \
                                                   --enable-libx264 ${x265:+--enable-libx265} --enable-libvpx
make -j$(num_cpus)
cd ../..
