#!/bin/sh

FFDIR=ffmpeg
FFOBJ=$FFDIR/bench-obj
FFMPEG=$FFOBJ/ffmpeg_g
FFPROBE=$FFOBJ/ffprobe_g

TIME=/usr/bin/time

REV=$(cd "$FFDIR" && git rev-parse --short HEAD)
[ -f NAME ] || { echo "No NAME file, please create this file with the bench platform name"; return 1; }
NAME=$(cat NAME)
BENCH_NAME="$NAME-$REV"

bench(){
  NAME=$1
  REPEAT=$2
  COMMAND=$3
  printf >&2 "$NAME"
  for i in $(seq $REPEAT); do
  printf >&2 "."
    $TIME -f "$NAME,%e,%U,%S" -o "${BENCH_NAME}.log" -a $COMMAND >/dev/null 2>&1
  done
  printf >&2 "\n"
}

bench_decode(){
  bench "decode_$1" 10 "$FFMPEG -hide_banner -threads 1 -i $2 -benchmark -f null -"
}

for name in alpha beta charlie; do
  for s in 960x540 1280x720 1920x1080 3840x2160; do
    for depth in yuv420p yuv420p10le yuv410p12le; do
      bench_decode "${name}_${s}_${depth}" "/mnt/alpha/samples/nocturne_${name}_${s}_${depth}.mkv"
    done
  done
done
